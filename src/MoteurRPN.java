import java.util.EmptyStackException;
import java.util.Stack;
import java.util.regex.Pattern;

import static java.lang.Math.abs;

public class MoteurRPN {
    private Stack<Double> pile;
    private String operateur;
    private double MAX_VALUE = 25;
    private double MIN_VALUE = 0;

    public MoteurRPN(){
        this.pile = new Stack<>();
        this.operateur = null;
    }

    /**
     * ajoute une operande à la pile, si l'operande donnée est en fait un operateur le calcul est effetué
     * @param op double
     */
    public void addOp(String op){
        try {
            if(op.equals("+") || op.equals("-") || op.equals("/") || op.equals("*")){
                this.operateur = op;
            }
            else if(Pattern.matches("[-+]?[0-9]*\\.?[0-9]+",op)){ //test si c'est un double
                if(abs(Double.parseDouble(op)) >= MIN_VALUE && abs(Double.parseDouble(op)) <= MAX_VALUE){
                    this.pile.push(Double.parseDouble(op));
                }
                else{
                    System.out.println("Wrong Input!  MAX_VALUE = " + MAX_VALUE +" et MIN_VALUE = " + MIN_VALUE );
                }
            }
            else {
                throw new ExceptionRPN("L'argument n'est pas un double ou un operateur.");
            }
        }
        catch(ExceptionRPN arg){
            System.out.println("L'argument n'est pas un double ou un operateur.");
        }
        finally {
            if(this.operateur != null){
                switch (this.operateur){
                    case "+":
                        this.result(Operation.PLUS);
                        break;
                    case "-":
                        this.result(Operation.MOINS);
                        break;
                    case "/":
                        this.result(Operation.DIV);
                        break;
                    case "*":
                        this.result(Operation.MULT);
                        break;
                }
                this.operateur = null;
            }
        }
    }

    /**
     * Calcul le resultat d'une operation sur deux operandes de la pile et ajoute le resultat sur la pile
     * Si il manque un argument dans la pile la pile est regenerée a son etat précédent
     * Division par zero entraine l'arret du programme
     * @param operation Operation
     */
    private void result(Operation operation){
            double op1 = Double.NaN;
            double op2 = Double.NaN;
            try{ //bloc pour pile vide
                op1 = this.pile.pop();
                op2 = this.pile.pop();
            }
            catch (EmptyStackException e){
                System.out.println("La n'est pas assez remplie pour effectuer une opération");
            }
            finally { //si il manquait un element regenere la pile avant les pop
                if(Double.isNaN(op2)){
                    this.pile.push(op1);
                }
            }
            if (operation.eval(op1, op2) == Double.NEGATIVE_INFINITY || operation.eval(op1, op2) == Double.POSITIVE_INFINITY || Double.isNaN(operation.eval(op1, op2))) {
                throw new ArithmeticException("Division par zero !");
            } else {
                this.pile.push(operation.eval(op1, op2));
            }
    }

    /**
     * getter de la pile (retourner l’ensemble des opérandes stockées)
     * @return Stack
     */
    public Stack<Double> getPile(){
        return this.pile;
    }

    public void setMAX_VALUE(double MAX_VALUE) {
        this.MAX_VALUE = MAX_VALUE;
    }

    public void setMIN_VALUE(double MIN_VALUE) {
        this.MIN_VALUE = MIN_VALUE;
    }

}