public enum CalculatriceRPN {
    CALCUL(){
        public void run() {
            System.out.println("Entrer un calcul en notation RPN ou \"exit\" pour quitter");
            String input = this.getSaisie().run();
            this.getSaisie().getMoteur().setMIN_VALUE(0);
            this.getSaisie().getMoteur().setMAX_VALUE(20);
            while(!(input.equals("exit"))) {
                if (input.equals("+") || input.equals("-") || input.equals("/") || input.equals("*")) {
                    if (this.getSaisie().getMoteur().getPile().size() < 2) {
                        System.out.println("Il n'y a pas assez d'opérandes");
                    }
                    else{
                        this.getSaisie().getMoteur().addOp(input);
                    }
                }
                else {
                    this.getSaisie().getMoteur().addOp(input);
                }
                System.out.println(this.getSaisie().getMoteur().getPile());
                input = this.getSaisie().run();
            }
        }
    };

    private SaisieRPN saisie;

    CalculatriceRPN(){
        this.saisie = new SaisieRPN();
    }

    public SaisieRPN getSaisie(){
        return this.saisie;
    }

    public abstract void run();

    public static void main(String[] args) {
        CalculatriceRPN.CALCUL.run();
    };

}
