public enum Operation {
    PLUS("+"){
        public double eval(double op1, double op2){
            return op1 + op2;
        }
    },
    MOINS("-"){
        public double eval(double op1, double op2){
            return op2 - op1;
        }
    },
    MULT("*"){
        public double eval(double op1, double op2){
            return op1*op2;
        }
    },
    DIV("/"){
        public double eval(double op1, double op2){
            return op2/op1;
        }
    };

    private String symbole;
    Operation(String symbole){
        this.symbole = symbole;
    }

    public abstract double eval(double op1, double op2);
}
