import java.util.Scanner;

public class SaisieRPN {
    private Scanner scanner;
    private MoteurRPN moteur;

    public SaisieRPN(){
        this.scanner = new Scanner(System.in);
        this.moteur = new MoteurRPN();
    }

    public String run() {
        String input;
        input=scanner.nextLine();
        return input;
    }

    public MoteurRPN getMoteur() {
        return moteur;
    }
}
