# Calculatrice RPN #
Exercice 6.5.1 POO avec Junit4 et Git

### Exceptions
                        Runtime Exception
                                ||
         Arithmetic Exception        Illegal Argument Exception
                                                   ||
                                        NumberFormat Exception

### Tests
- [x] addtion fonctionne
    - [x] pos + pos
    - [x] pos + neg
    - [x] neg + neg
- [x] soustraction fonctionne
    - [x] pos - pos
    - [x] pos - neg
    - [x] neg - neg
- [x] multiplication fonctionne
    - [x] pos * pos
    - [x] pos * neg
    - [x] neg * neg
- [x] division fonctionne
    - [x] pos / pos
    - [x] pos / neg
    - [x] pos / neg
    - [x] neg / neg
- [x] test de combinaison d'operations
    - [x] 2 * (3 + 4) = 14, input = 2 3 4 + *
    - [x] -12 / (12 * 1) = -1, input = -12 12 1 * /
    - [x] -6/(6-(2/(3*5))) = -1.02272727273, input = -6 6 2 3 5 * / - /
    - [x] ((15 / (7 - (1 + 1))) / 3) - (2 + (1 + 1)) = 5, input = 15 7 1 1 + - / 3 * 2 1 1 + + -
- [x] division par 0 -> Arithmetic Exception -> fin du programme
- [x] MIN_VALUE
- [x] MAX_VALUE
- [x] parametre manquant -> exception caught
- [x] Entrée non reconnue par le programme -> exception caught