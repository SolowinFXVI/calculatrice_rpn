import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.EmptyStackException;

import static org.junit.Assert.*;

public class CalculatriceRPNTest {

    @Test
    public void addPosPos(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("3");
        m.addOp("5");
        m.addOp("+");
        assertTrue(m.getPile().pop() == 8.0);
    }

    @Test
    public void addPosNeg(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("2");
        m.addOp("-5");
        m.addOp("+");
        assertTrue(m.getPile().pop() == -3.0);
    }

    @Test
    public void addNegNeg(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("-2");
        m.addOp("-3");
        m.addOp("+");
        assertTrue(m.getPile().pop() == -5.0);
    }

    @Test
    public void subPosPos(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("3");
        m.addOp("5");
        m.addOp("-");
        assertTrue(m.getPile().pop() == -2.0);
    }

    @Test
    public void subPosNeg(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("2");
        m.addOp("-5");
        m.addOp("-");
        assertTrue(m.getPile().pop() == 7.0);
    }

    @Test
    public void subNegNeg(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("-2");
        m.addOp("-3");
        m.addOp("-");
        assertTrue(m.getPile().pop() == 1.0);
    }

    @Test
    public void mulPosPos(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("3");
        m.addOp("5");
        m.addOp("*");
        assertTrue(m.getPile().pop() == 15.0);
    }

    @Test
    public void mulPosNeg(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("2");
        m.addOp("-5");
        m.addOp("*");
        assertTrue(m.getPile().pop() == -10.0);
    }

    @Test
    public void mulNegNeg(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("-2");
        m.addOp("-3");
        m.addOp("*");
        assertTrue(m.getPile().pop() == 6.0);
    }

    @Test
    public void divPosPos(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("3");
        m.addOp("5");
        m.addOp("/");
        assertTrue(m.getPile().pop() == 0.6);
    }

    @Test
    public void divPosNeg(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("2");
        m.addOp("-5");
        m.addOp("/");
        assertTrue(m.getPile().pop() == -0.4);
    }

    @Test
    public void divNegNeg(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("-2");
        m.addOp("-4");
        m.addOp("/");
        assertTrue(m.getPile().pop() == 0.5);
    }

    @Test //2 * (3 + 4) = 14, input = 2 3 4 + *
    public void combinaison1(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("2");
        m.addOp("3");
        m.addOp("4");
        m.addOp("+");
        m.addOp("*");
        assertTrue(m.getPile().pop() == 14);
    }

    @Test //-12 / (12 * 1) = -1, input = -12 12 1 * /
    public void combinaison2(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("-12");
        m.addOp("12");
        m.addOp("1");
        m.addOp("*");
        m.addOp("/");
        assertTrue(m.getPile().pop() == -1);
    }

    @Test //-6/(6-(2/(3*5))) = -1.02272727273, input = -6 6 2 3 5 * / - /
    public void combinaison3(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("-6");
        m.addOp("6");
        m.addOp("2");
        m.addOp("3");
        m.addOp("5");
        m.addOp("*");
        m.addOp("/");
        m.addOp("-");
        m.addOp("/");
        assertTrue(m.getPile().pop() == -1.0227272727272727);
    }

    @Test //((15 / (7 - (1 + 1))) / 3) - (2 + (1 + 1)) = 5, input = 15 7 1 1 + - / 3 * 2 1 1 + + -
    public void combinaison4(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("15");
        m.addOp("7");
        m.addOp("1");
        m.addOp("1");
        m.addOp("+");
        m.addOp("-");
        m.addOp("/");
        m.addOp("3");
        m.addOp("*");
        m.addOp("2");
        m.addOp("1");
        m.addOp("1");
        m.addOp("+");
        m.addOp("+");
        m.addOp("-");
        assertTrue(m.getPile().pop() == 5);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Test
    public  void divideByZero(){
        MoteurRPN m = new MoteurRPN();
        thrown.expect(ArithmeticException.class);
        thrown.expectMessage("Division par zero !");
        m.addOp("3");
        m.addOp("3");
        m.addOp("3");
        m.addOp("-");
        m.addOp("/");
    }

    @Test (expected = EmptyStackException.class)
    public void minValue(){
        MoteurRPN m = new MoteurRPN();
        m.setMIN_VALUE(12);
        m.addOp("11");
        m.getPile().pop();
    }

    @Test (expected = EmptyStackException.class)
    public void maxValue(){
        MoteurRPN m = new MoteurRPN();
        m.setMAX_VALUE(3);
        m.addOp("4");
        m.getPile().pop();
    }

    @Test
    public void missingParameter(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("");
        m.addOp("3");
        m.addOp("3");
        m.addOp("+");
        assertTrue(m.getPile().pop() == 6);
    }

    @Test
    public void wrongParameter(){
        MoteurRPN m = new MoteurRPN();
        m.addOp("test");
        m.addOp("3");
        m.addOp("3");
        m.addOp("+");
        assertTrue(m.getPile().pop() == 6);
    }

}